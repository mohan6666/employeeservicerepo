package com.employee.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.FetchEmployeeResponseDto;
import com.employee.dto.UpdateRequestDto;
import com.employee.entity.Employee;
import com.employee.service.EmpolyeeService;
import com.employee.service.FetchEmployeeService;
import com.employee.service.RemoveEmployeeService;
import com.employee.service.UpdateEmployeeService;


@Validated
@RestController
@RequestMapping("/employee")
public class employeeController {

	@Autowired
	EmpolyeeService empolyeeService;

	@Autowired
	FetchEmployeeService fetchEmployeeService;

	@Autowired
	RemoveEmployeeService removeEmployeeService;

	@Autowired
	UpdateEmployeeService updateEmployeeService;



	/**
	 * 
	 * @param employeeRequestDto
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/addemployees")
	public ResponseEntity<String> addEmployee(@Valid @RequestBody EmployeeRequestDto employeeRequestDto)throws Exception {
		
		return new ResponseEntity<String>(empolyeeService.addEmployee(employeeRequestDto), HttpStatus.CREATED);
	}

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param Status
	 * @return
	 */
	@GetMapping("/fetchAllemployee")
	public ResponseEntity<List<FetchEmployeeResponseDto>> fetchEmployee(@RequestParam int pageNumber,
			@RequestParam int pageSize, @RequestParam String Status) {
		List<FetchEmployeeResponseDto> fetchEmployeeResponseDto = fetchEmployeeService.fetchEmployee(pageNumber,
				pageSize, Status);
		return new ResponseEntity<List<FetchEmployeeResponseDto>>(fetchEmployeeResponseDto, HttpStatus.ACCEPTED);
	}

	/**
	 * 
	 * @param employeeCode
	 * @return
	 */
	@DeleteMapping("/removeeployee")

	public ResponseEntity<String> removeEmployee(@RequestParam Long employeeCode) {

		return new ResponseEntity<String>(removeEmployeeService.removeEmployee(employeeCode), HttpStatus.ACCEPTED);

	}

	/**
	 * 
	 * @param updateRequestDto
	 * @return
	 */
	@PatchMapping("/updateemployee")
	public ResponseEntity<String> updateEmployee(@RequestBody UpdateRequestDto updateRequestDto) {

		return new ResponseEntity<String>(updateEmployeeService.updateEmployee(updateRequestDto), HttpStatus.ACCEPTED);

	}

	/**
	 * 
	 * @param employeeCode
	 * @return
	 */
	
	//@GetMapping("/getemployee/{employeeCode}")
//	public ResponseEntity<Employee> getEmployeeByCode(@RequestParam Long employeeCode) {
//		
//		return new ResponseEntity<Employee>(fetchEmployeeService.getEmployeeByCode(employeeCode), HttpStatus.OK);
//
//	}
	
	@GetMapping("/getemployee/{employeeCode}")
	public ResponseEntity<FetchEmployeeResponseDto> getEmployeeByCode(@RequestParam Long employeeCode) {
		FetchEmployeeResponseDto fetchEmployeeResponseDto =fetchEmployeeService.getEmployeeByCode(employeeCode);
		//if(fetchEmployeeResponseDto!=null) {
		//	System.out.println("emptyObject"+fetchEmployeeResponseDto);
			return new ResponseEntity<FetchEmployeeResponseDto>(fetchEmployeeResponseDto, HttpStatus.OK);
			
		//}
		
	//	return new ResponseEntity<FetchEmployeeResponseDto>(fetchEmployeeResponseDto, HttpStatus.BAD_REQUEST);
	}

}
