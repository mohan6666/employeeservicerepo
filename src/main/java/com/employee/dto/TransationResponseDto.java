package com.employee.dto;

/**
 * @author mohan
 *
 */
public class TransationResponseDto {
private String message;

public String getMessage() {
	return message;
}

public void setMessage(String message) {
	this.message = message;
}

}
