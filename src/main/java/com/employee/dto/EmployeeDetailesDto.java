package com.employee.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


/**
 * @author mohan
 *
 */
public class EmployeeDetailesDto {

	@Digits(integer = 8, fraction = 1, message = "InValid employee code")
	private Long employeeCode;
	
	@NotEmpty(message = "employeeName should not be empty")
	@Size(min = 3, max = 100,message = "employee name should by 100 character only")
	private String employeeName;

	@NotEmpty(message = "gender should not be empty")
	@Size(min = 0, max = 1,message = "employee gender should by 1 character only")
	private String employeeGender;

	@NotEmpty(message = "designation should not be empty")
	@Size(min = 5, max = 100, message = "EmailId size should should be 100 characterstics only")
	private String designation;

	@NotEmpty(message = "emailId should not be empty")
	@Email
	@Size(min = 5, max = 100,message = "EmailId size should should be 100 characterstics only")
	private String emailId;

	@NotEmpty(message = "experience should not be empty")
	@NotNull(message = "please provide valid experience")
	@Size(min = 1, max = 3, message = "please provide valid experience")
	private String experience;

	@NotEmpty(message = "Please provide a Mobile Number")
	@Pattern(regexp = "(^$|[0-9]{10})", message = "Provide valid Mobile Number")
	@Size(min = 5, max = 10, message = "phone number is of 10 digits")
	private String phoneNumber;

	@NotEmpty(message = "location should not be empty")
	private String location;

	public Long getEmployeeCode() {
		return employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public String getEmployeeGender() {
		return employeeGender;
	}

	public String getDesignation() {
		return designation;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getExperience() {
		return experience;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setEmployeeCode(Long employeeCode) {
		this.employeeCode = employeeCode;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public void setEmployeeGender(String employeeGender) {
		this.employeeGender = employeeGender;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
