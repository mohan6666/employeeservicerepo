package com.employee.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.dto.FetchEmployeeResponseDto;
import com.employee.entity.Employee;

/**
 * @author mohan
 *
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	Page<Employee> findBystatus(Pageable pageable, String Status);

	//Employee findByEmployeeCode(Long employeeCode);



	

	

}
