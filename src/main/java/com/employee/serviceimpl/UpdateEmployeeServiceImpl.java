package com.employee.serviceimpl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.UpdateRequestDto;
import com.employee.entity.Employee;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repository.EmployeeRepository;
import com.employee.service.UpdateEmployeeService;

/**
 * @author mohan
 *
 */
@Service
public class UpdateEmployeeServiceImpl implements UpdateEmployeeService {
	@Autowired
	EmployeeRepository employeeRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateEmployeeServiceImpl.class);
	
	@Override
	public String updateEmployee(UpdateRequestDto updateRequestDto) {
		// TODO Auto-generated method stub
		Optional<Employee> employeeRecord;
		employeeRecord = employeeRepository.findById(updateRequestDto.getEmployeeCode());
		
		if (!employeeRecord.isPresent()) {
			LOGGER.warn("Invalid User");
			throw new EmployeeNotFoundException("Employee Id not found");
		}
		
		Employee employee = employeeRecord.get();
		employee.setDesignation(updateRequestDto.getDesignation());
		employee.setEmailId(updateRequestDto.getEmailId());
		employee.setEmployeeCode(updateRequestDto.getEmployeeCode());
		employee.setEmployeeGender(updateRequestDto.getEmployeeGender());
		employee.setEmployeeName(updateRequestDto.getEmployeeName());
		employee.setExperience(updateRequestDto.getExperience());
		employee.setLocation(updateRequestDto.getLocation());
		employee.setPhoneNumber(updateRequestDto.getPhoneNumber());
		employee.setStatus(updateRequestDto.getStatus());
		employeeRepository.save(employee);

		return "Updated succesfull";
	}

}
