
package com.employee.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author mohan
 *
 */
public class EmployeeRequestDto {
	
	@NotEmpty(message = "status should not be empty")
	@Size(min = 3, max = 15, message = "employee status should be 15 charaters only")
	private String status;
	
	@NotEmpty(message = "list should not be empty")
	private List<@Valid EmployeeDetailesDto> employeeDetailesDto= new ArrayList<EmployeeDetailesDto>();

	public String getStatus() {
		return status;
	}

	public List<EmployeeDetailesDto> getEmployeeDetailesDto() {
		return employeeDetailesDto;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setEmployeeDetailesDto(List<EmployeeDetailesDto> employeeDetailesDto) {
		this.employeeDetailesDto = employeeDetailesDto;
	}

	
}