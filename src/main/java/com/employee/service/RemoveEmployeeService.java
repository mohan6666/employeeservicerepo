package com.employee.service;

/**
 * @author mohan
 *
 */
public interface RemoveEmployeeService {

	String removeEmployee(Long employeeCode);

}
