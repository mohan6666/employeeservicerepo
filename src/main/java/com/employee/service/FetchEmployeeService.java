package com.employee.service;

import java.util.List;

import com.employee.dto.FetchEmployeeResponseDto;
import com.employee.entity.Employee;

/**
 * @author mohan
 *
 */
public interface FetchEmployeeService {

	List<FetchEmployeeResponseDto> fetchEmployee(int pageNumber, int pageSize, String Status);

	FetchEmployeeResponseDto getEmployeeByCode(Long employeeCode);

//	Employee getEmployeeByCode(Long employeeCode);

	

	

}
