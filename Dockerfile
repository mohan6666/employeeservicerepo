FROM openjdk:latest
Copy *.jar employee-0.0.1-SNAPSHOT.jar
EXPOSE 1500
ENTRYPOINT ["java","-jar","employee-0.0.1-SNAPSHOT.jar"]
