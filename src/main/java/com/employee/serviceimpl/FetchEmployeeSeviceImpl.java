package com.employee.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.employee.dto.FetchEmployeeResponseDto;
import com.employee.entity.Employee;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repository.EmployeeRepository;
import com.employee.service.FetchEmployeeService;

/**
 * @author mohan
 *
 */
@Service
public class FetchEmployeeSeviceImpl implements FetchEmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	private static final Logger LOGGER = LoggerFactory.getLogger(FetchEmployeeSeviceImpl.class);

	@Override
	public List<FetchEmployeeResponseDto> fetchEmployee(int pageNumber, int pageSize, String Status) {

		Page<Employee> employee;
		List<FetchEmployeeResponseDto> fetchEmployeeResponceDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		employee = employeeRepository.findBystatus(pageable, Status);

		if (employee.isEmpty()) {
			LOGGER.warn("Invalid User");
			throw new EmployeeNotFoundException("No Employee Found");
		}

		employee.stream().forEach(emp -> {
			FetchEmployeeResponseDto fetchEmployeeResponseDto = new FetchEmployeeResponseDto();
			BeanUtils.copyProperties(emp, fetchEmployeeResponseDto);
			fetchEmployeeResponceDtoList.add(fetchEmployeeResponseDto);

		});

		return fetchEmployeeResponceDtoList;
	}

	@Override
	public FetchEmployeeResponseDto getEmployeeByCode(Long employeeCode) {
		// TODO Auto-generated method stub
		//FetchEmployeeResponseDto fetchEmployeeResponseDto = null;
		Optional<Employee> employee;

		employee = employeeRepository.findById(employeeCode);

		//try {
			if (!employee.isPresent()) {
				throw new EmployeeNotFoundException("Invalid employeecode");

			}
		//} catch (EmployeeNotFoundException e) {
			// TODO: handle exception
			// empty object
			
		//	System.out.println("emptyObject"+fetchEmployeeResponseDto);
		//	return fetchEmployeeResponseDto;
	//	}
		FetchEmployeeResponseDto fetchEmployeeResponseDto = new FetchEmployeeResponseDto();
		Employee employees = employee.get();
		BeanUtils.copyProperties(employees, fetchEmployeeResponseDto);

		return fetchEmployeeResponseDto;

	}

//	@Override
//	public Employee getEmployeeByCode(Long employeeCode) {
//		// TODO Auto-generated method stub
//		return employeeRepository.findByEmployeeCode(employeeCode);
//	}

}
