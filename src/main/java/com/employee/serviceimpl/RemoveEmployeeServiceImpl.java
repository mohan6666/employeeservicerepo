package com.employee.serviceimpl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.entity.Employee;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repository.EmployeeRepository;
import com.employee.service.RemoveEmployeeService;

/**
 * @author mohan
 *
 */
@Service
public class RemoveEmployeeServiceImpl implements RemoveEmployeeService {
	@Autowired
	EmployeeRepository employeeRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoveEmployeeServiceImpl.class);

	@Override
	public String removeEmployee(Long employeeCode) {
		Optional<Employee> employeeRecord;
		employeeRecord = employeeRepository.findById(employeeCode);

		if (!employeeRecord.isPresent()) {

			LOGGER.warn("Invalid User");
			throw new EmployeeNotFoundException("Employee not found");
		}

		Employee employee = employeeRecord.get();
		employee.setStatus("Inactive");
		employeeRepository.save(employee);
		return "deleted succesfull";
	}

}
