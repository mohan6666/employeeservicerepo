package com.employee.service;

import com.employee.dto.UpdateRequestDto;

/**
 * @author mohan
 *
 */
public interface UpdateEmployeeService {

	String updateEmployee(UpdateRequestDto updateRequestDto);

}
