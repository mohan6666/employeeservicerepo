package com.employee.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.EmployeeDetailesDto;
import com.employee.dto.EmployeeRequestDto;
import com.employee.entity.Employee;
import com.employee.repository.EmployeeRepository;
import com.employee.service.EmpolyeeService;

/**
 * @author mohan
 *
 */
@Service
public class EmployeeServiceImpl implements EmpolyeeService {

	@Autowired
	EmployeeRepository employeeRepository;


	@Override
	public String addEmployee(EmployeeRequestDto employeeRequestDto) throws Exception {
		// TODO Auto-generated method stub
		String status=employeeRequestDto.getStatus();
		List<Employee> employeeList=new ArrayList<Employee>();
		for(EmployeeDetailesDto employeeDto:employeeRequestDto.getEmployeeDetailesDto()) {
			Employee employee=new Employee();
			BeanUtils.copyProperties(employeeDto, employee);
			employee.setStatus(status);
			employeeList.add(employee);
		}
//		employeeRequestDto.getEmployeeDetailesDto().stream().forEach(employeeDto ->{
//			Employee employee=new Employee();
//			BeanUtils.copyProperties(employeeDto, employee);
//			employee.setStatus(status);
//			employeeList.add(employee);
//			
//		});
		employeeRepository.saveAll(employeeList);
		
		return "success";
	}

	
}
